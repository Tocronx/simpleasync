﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Tocronx.SimpleAsync.Tests.Helper;

public class RetryServiceTests
{
    private readonly RetryService retryService = new();

    [Fact]
    public async Task Retry_Successful()
    {
        var expected = "Hello World";
        var result = await retryService.Retry(ct => Task.FromResult(expected));
        Assert.Equal(expected, result);
    }

    [Fact]
    public async Task Retry_ThrowsException()
    {
        var expected = new Exception("Test Exception");
        await Assert.ThrowsAsync<Exception>(() => retryService.Retry(ct => Task.FromException<string>(expected)));
    }

    [Fact]
    public async Task Retry_RetryWorks()
    {
        var expected = "Hello World";
        var count = 0;
        var result = await retryService.Retry(ct =>
        {
            if (count == 0)
            {
                count++;
                throw new Exception("Test Exception");
            }
            else
            {
                return Task.FromResult(expected);
            }
        });
        Assert.Equal(expected, result);
    }

    [Fact]
    public async Task Retry_CancellationToken()
    {
        await Assert.ThrowsAsync<TaskCanceledException>(() => retryService.Retry(ct => Task.FromCanceled<string>(ct), cancellationToken: new CancellationToken(true)));
    }
}
