﻿using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace Tocronx.SimpleAsync.Tests.Helper;

public class AsyncLockTest
{
    private readonly AsyncLock asyncLock = new();
    private int number;
    private readonly ITestOutputHelper testOutputHelper;

    public AsyncLockTest(ITestOutputHelper testOutputHelper)
    {
        this.testOutputHelper = testOutputHelper;
    }

    [Fact]
    public async Task AsyncLockCalculationTest()
    {
        var task = Task.Run(async () =>
        {
            await Task.Delay(100);
            //testOutputHelper.WriteLine("Thread Try");
            using (var l = await asyncLock.GetLookAsync())
            {
                testOutputHelper.WriteLine("Thread");
                number *= 10;
                await Task.Delay(200);
            }
            await Task.Delay(100);
            //testOutputHelper.WriteLine("Thread Try");
            using (var l = await asyncLock.GetLookAsync())
            {
                testOutputHelper.WriteLine("Thread");
                number *= 10;
                await Task.Delay(200);
            }
            await Task.Delay(100);
            //testOutputHelper.WriteLine("Thread Try");
            using (var l = await asyncLock.GetLookAsync())
            {
                testOutputHelper.WriteLine("Thread");
                number *= 10;
                await Task.Delay(200);
            }
        });
        //testOutputHelper.WriteLine("Main Try");
        using (var l = asyncLock.GetLook())
        {
            testOutputHelper.WriteLine("Main");
            number += 5;
            await Task.Delay(200);
        }
        await Task.Delay(100);
        //testOutputHelper.WriteLine("Main Try");
        using (var l = asyncLock.GetLook())
        {
            testOutputHelper.WriteLine("Main");
            await Task.Delay(200);
            number += 5;
        }
        await Task.Delay(100);
        //testOutputHelper.WriteLine("Main Try");
        using (var l = asyncLock.GetLook())
        {
            testOutputHelper.WriteLine("Main");
            number += 5;
            await Task.Delay(200);
        }
        await task;
        Assert.Equal(/* (((0+5)*10+5)*10+5)*10 = */ 5550, number);
    }
}
