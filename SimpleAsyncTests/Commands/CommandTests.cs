﻿using System;
using Xunit;

namespace Tocronx.SimpleAsync.Tests.Commands;

public class CommandTests
{

    [Fact]
    public void CommandTest()
    {
        var actionCalled = false;
        void action()
        {
            actionCalled = true;
        }
        var command = new Command(action);

        Assert.True(command.CanExecute(null));
        Assert.True(command.IsExecutable);
        command.Execute(null);
        Assert.True(command.CanExecute(null));
        Assert.True(command.IsExecutable);
        Assert.True(actionCalled);
    }

    [Fact]
    public void GenericAsyncCommandTest()
    {
        var actionCalled = false;
        void action(bool parameter)
        {
            actionCalled = parameter;
        }
        var command = new Command<bool>(action);

        Assert.True(command.CanExecute(null));
        Assert.True(command.IsExecutable);
        command.Execute(true);
        Assert.True(command.CanExecute(null));
        Assert.True(command.IsExecutable);
        Assert.True(actionCalled);
    }

    [Fact]
    public void GenericNullCheckingCommandTest()
    {
        var actionCalled = false;
        void action(string parameter)
        {
            actionCalled = !string.IsNullOrEmpty(parameter);
        }
        var command = new NullCheckingCommand<string>(action);
        // Act & Assert
        Assert.True(command.CanExecute(null));
        Assert.True(command.IsExecutable);
        command.Execute("Parameter");
        Assert.True(command.CanExecute(null));
        Assert.True(command.IsExecutable);
        Assert.True(actionCalled);
        Assert.Throws<NullReferenceException>(() => command.Execute(null));
    }

    [Fact]
    public void IsExecutableTest()
    {
        var command = new Command(() => { });
        Assert.True(command.CanExecute(null));
        Assert.True(command.IsExecutable);
        command.IsExecutable = false;
        Assert.False(command.CanExecute(null));
        Assert.False(command.IsExecutable);
        command.IsExecutable = true;
        Assert.True(command.CanExecute(null));
        Assert.True(command.IsExecutable);
    }

}
