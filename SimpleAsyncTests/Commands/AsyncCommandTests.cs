﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using Xunit;

namespace Tocronx.SimpleAsync.Tests.Commands;

public class AsyncCommandTests
{

    [Fact]
    public async Task AsyncCommandTest()
    {
        // Arrange
        TaskCompletionSource tsc = new();
        var actionCalled = false;
        Task action()
        {
            actionCalled = true;
            return tsc.Task;
        }
        var command = new AsyncCommand(action);
        // Act & Assert
        Assert.True(command.CanExecute(null));
        Assert.True(command.IsExecutable);
        var task = command.ExecuteAsync(null);
        Assert.False(command.IsExecutable);
        Assert.False(command.CanExecute(null));
        tsc.SetResult();
        await task;
        Assert.True(command.CanExecute(null));
        Assert.True(command.IsExecutable);
        Assert.True(actionCalled);
    }

    [Fact]
    public async Task GenericAsyncCommandTest()
    {
        // Arrange
        TaskCompletionSource tsc = new();
        var actionCalled = false;
        Task action(bool parameter)
        {
            actionCalled = parameter;
            return tsc.Task;
        }
        var command = new AsyncCommand<bool>(action);
        // Act & Assert
        Assert.True(command.CanExecute(null));
        Assert.True(command.IsExecutable);
        var task = command.ExecuteAsync(true);
        Assert.False(command.IsExecutable);
        Assert.False(command.CanExecute(null));
        tsc.SetResult();
        await task;
        Assert.True(command.CanExecute(null));
        Assert.True(command.IsExecutable);
        Assert.True(actionCalled);
    }

    [Fact]
    public async Task GenericNullCheckingAsyncCommandTest()
    {
        // Arrange
        TaskCompletionSource tsc = new();
        var actionCalled = false;
        Task action(string parameter)
        {
            actionCalled = !string.IsNullOrEmpty(parameter);
            return tsc.Task;
        }
        var command = new AsyncNullCheckingCommand<string>(action);
        // Act & Assert
        Assert.True(command.CanExecute(null));
        Assert.True(command.IsExecutable);
        var task = command.ExecuteAsync("Parameter");
        Assert.False(command.IsExecutable);
        Assert.False(command.CanExecute(null));
        tsc.SetResult();
        await task;
        Assert.True(command.CanExecute(null));
        Assert.True(command.IsExecutable);
        Assert.True(actionCalled);
        await Assert.ThrowsAsync<NullReferenceException>(async () => await command.ExecuteAsync(null));
    }

    [Fact]
    public void AsyncCommandParallelTest()
    {
        var tsc = new TaskCompletionSource<bool>();
        var count = 0;
        var command = new AsyncCommand(() => { count++; return tsc.Task; })
        {
            CanExecuteParallel = true
        };
        Assert.True(command.CanExecute(null));
        Assert.True(command.IsExecutable);
        command.Execute(null);
        Assert.True(command.CanExecute(null));
        Assert.True(command.IsExecutable);
        command.Execute(null);
        Assert.Equal(2, count);
        tsc.SetResult(true);
    }

    [Fact]
    public void IsExecutableTest()
    {
        var command = new AsyncCommand(() => Task.CompletedTask);
        Assert.True(command.CanExecute(null));
        Assert.True(command.IsExecutable);
        command.IsExecutable = false;
        Assert.False(command.CanExecute(null));
        Assert.False(command.IsExecutable);
        command.IsExecutable = true;
        Assert.True(command.CanExecute(null));
        Assert.True(command.IsExecutable);
    }

    [Fact]
    public void CanExecuteBindingTest()
    {
        var command = new AsyncCommand(() => Task.CompletedTask);
        var obj = new BindingTest();
        command.SetCanExecuteBinding(() => obj.Ready);
        Assert.True(command.CanExecute(null));
        Assert.True(command.IsExecutable);
        obj.Ready = false;
        Assert.False(command.CanExecute(null));
        Assert.False(command.IsExecutable);
        obj.Ready = true;
        Assert.True(command.CanExecute(null));
        Assert.True(command.IsExecutable);
    }

    private class BindingTest : INotifyPropertyChanged
    {
        private bool ready = true;
        public bool Ready
        {
            get => ready;
            set
            {
                ready = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Ready)));
            }
        }

        public event PropertyChangedEventHandler? PropertyChanged;
    }

}
