﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Tocronx.SimpleAsync.EnumerableExtensions;
using Xunit;

namespace Tocronx.SimpleAsync.Tests.EnumerableExtensions;

public class AsyncEnumerableExtensionTest
{
    [Fact]
    public async Task ParallelSelectAsync()
    {
        List<int> list = [501, 502, 503, 504, 505, 506];
        var asyncEnumerable = list.ParallelSelectAsync(x => Task.FromResult(x));
        var sw = Stopwatch.StartNew();
        var result = await asyncEnumerable.ParallelSelectAsync(async x => { await Task.Delay(x); return x; }, 3).ToListAsync();
        sw.Stop();
        Assert.Equal(list.Sum(), result.Sum());
        Assert.True(sw.ElapsedMilliseconds - 1000 < 50);
        Assert.Equal(list.Count, result.Count());
    }
}
