﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Tocronx.SimpleAsync.EnumerableExtensions;
using Xunit;

namespace Tocronx.SimpleAsync.Tests.EnumerableExtensions;

public class EnumerableExtensionsTest
{
    private volatile int counter;

    [Fact]
    public async Task ForEachAsync()
    {
        var list = new List<int>() { 501, 502, 503, 504, 505, 506 };
        var sw = Stopwatch.StartNew();
        var result = await list.ForEachAsync(async x => { await Task.Delay(x); return x; }, 3);
        sw.Stop();
        Assert.Equal(list.Sum(), result.Sum());
        Assert.True(sw.ElapsedMilliseconds - 1000 < 50);
        Assert.Equal(list.Count, result.Count());
    }

    [Fact]
    public async Task PreloadingForEachTest()
    {
        var list = new List<int>() { 500, 500, 500, 500, 500 };
        var sw = Stopwatch.StartNew();
        var enumerable = list.ToPreloadingEnumerable(async x => { await Task.Delay(x); Interlocked.Increment(ref counter); return x; }, 3);
        foreach (var task in enumerable)
        {
            await task;
        }
        sw.Stop();
        Assert.True(sw.ElapsedMilliseconds - 1000 < 50);
        Assert.Equal(list.Count, counter);
    }

    [Fact]
    public async Task ParallelForEachOld()
    {
        var list = new List<int>() { 501, 502, 503, 504, 505, 506 };
        var sw = Stopwatch.StartNew();
        await list.ParallelForEach(async x => await Task.Delay(x), 4);
        sw.Stop();
        Assert.True(sw.ElapsedMilliseconds - 1000 < 50);
    }

    [Fact]
    public async Task ParallelForEach()
    {
        var list = new List<int>() { 501, 502, 503, 504, 505, 506 };
        var sw = Stopwatch.StartNew();
        await list.ParallelForEach(async x => await Task.Delay(x), 4, default);
        sw.Stop();
        Assert.True(sw.ElapsedMilliseconds - 1000 < 50);
    }

    [Fact]
    public async Task ParallelSelect()
    {
        var list = new List<int>() { 501, 502, 503, 504, 505, 506 };
        var sw = Stopwatch.StartNew();
        var result = await list.ParallelSelect(async x => { await Task.Delay(x); return x; }, 3);
        sw.Stop();
        Assert.Equal(list.Sum(), result.Sum());
        Assert.True(sw.ElapsedMilliseconds - 1000 < 50);
        Assert.Equal(list.Count, result.Count());
    }

    [Fact]
    public async Task ParallelSelectAsync()
    {
        var list = new List<int>() { 501, 502, 503, 504, 505, 506 };
        var sw = Stopwatch.StartNew();
        var result = await list.ParallelSelectAsync(async x => { await Task.Delay(x); return x; }, 3).ToListAsync();
        sw.Stop();
        Assert.Equal(list.Sum(), result.Sum());
        Assert.True(sw.ElapsedMilliseconds - 1000 < 50);
        Assert.Equal(list.Count, result.Count());
    }

}
