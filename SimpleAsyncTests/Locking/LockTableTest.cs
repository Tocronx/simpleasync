﻿using System.Threading.Tasks;
using Xunit;

namespace Tocronx.SimpleAsync.Tests.Locking;

public class LockTableTest
{
    [Fact]
    public async Task SimpleLockTableTest()
    {
        var lockTable = new LockTable<string>();
        lockTable.Lock("A");
        Assert.False(lockTable.TryLock("A"));
        using (lockTable.GetLock("B"))
        {
            Assert.False(lockTable.TryLock("B"));
            Assert.True(lockTable.TryLock("C"));
        }
        Assert.True(lockTable.TryLock("B"));
        lockTable.Unlock("A");
        Assert.True(lockTable.TryLock("A"));
        using (await lockTable.GetLockAsync("D"))
        {
            Assert.False(lockTable.TryLock("D"));
        }
        Assert.True(lockTable.TryLock("D"));
    }
}
