using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Xunit;

namespace Tocronx.SimpleAsync.Tests.TaskRunners;

public class CancelableTaskRunnerTest
{
    [Fact]
    public async Task CancelableTasksTest()
    {
        var sth = new CancelableTaskRunner(true);
        var i = 0;
        var sw = Stopwatch.StartNew();
        var t1 = sth.RunAndCancelPrevious(async x =>
        {
            await Task.Delay(500, x);
            if (!x.IsCancellationRequested) { Assert.Equal(0, i++); }
        });
        var t2 = sth.RunAndCancelPrevious(async x =>
        {
            await Task.Delay(1000, x);
            if (!x.IsCancellationRequested) { Assert.Equal(0, i++); }
        });
        var t3 = sth.RunAndCancelPrevious(async x =>
        {
            await Task.Delay(250, x);
            if (!x.IsCancellationRequested) { Assert.Equal(0, i++); }
        });
        await t3;
        Assert.True(Math.Abs(sw.ElapsedMilliseconds - 250) < 50);
        await Task.WhenAll(t1, t2, t3);
        Assert.True(t1.IsCompleted);
        Assert.True(t2.IsCompleted);
        Assert.True(t3.IsCompleted);
    }

    [Fact]
    public async Task CancelableTasksExceptionTest()
    {
        var sth = new CancelableTaskRunner(false);
        var i = 0;
        var sw = Stopwatch.StartNew();
        var t1 = sth.RunAndCancelPrevious(async x =>
        {
            await Task.Delay(500, x);
            if (!x.IsCancellationRequested) { Assert.Equal(0, i++); }
        });
        var t2 = sth.RunAndCancelPrevious(async x =>
        {
            await Task.Delay(1000, x);
            if (!x.IsCancellationRequested) { Assert.Equal(0, i++); }
        });
        var t3 = sth.RunAndCancelPrevious(async x =>
        {
            await Task.Delay(250, x);
            if (!x.IsCancellationRequested) { Assert.Equal(0, i++); }
        });
        await t3;
        Assert.True(Math.Abs(sw.ElapsedMilliseconds - 250) < 50);
        await Assert.ThrowsAsync<TaskCanceledException>(() => Task.WhenAll(t1, t2, t3));
        //await Assert.ThrowsAsync<OperationCanceledException>(() => Task.WhenAll(t1, t2, t3));
        Assert.True(t1.IsCanceled);
        Assert.True(t2.IsCanceled);
        Assert.True(t3.IsCompleted);
    }

    [Fact]
    public async Task CancelableTasksWithReturnValueTest()
    {
        var sth = new CancelableTaskRunner(true);
        var i = 0;
        var sw = Stopwatch.StartNew();
        var t1 = sth.RunAndCancelPrevious<int?>(async x =>
        {
            await Task.Delay(500, x);
            if (!x.IsCancellationRequested) { Assert.Equal(0, i++); return 1; }
            return null;
        });
        var t2 = sth.RunAndCancelPrevious<int?>(async x =>
        {
            await Task.Delay(1000, x);
            if (!x.IsCancellationRequested) { Assert.Equal(0, i++); return 2; }
            return null;
        });
        var t3 = sth.RunAndCancelPrevious<int?>(async x =>
        {
            await Task.Delay(250, x);
            if (!x.IsCancellationRequested) { Assert.Equal(0, i++); return 3; }
            return null;
        });
        await t3;
        Assert.True(Math.Abs(sw.ElapsedMilliseconds - 250) < 50);
        await Task.WhenAll(t1, t2, t3);
        Assert.True(t1.IsCompleted);
        Assert.True(t2.IsCompleted);
        Assert.True(t3.IsCompleted);
    }

    [Fact]
    public async Task CancelableTasksWithReturnValueExceptionTest()
    {
        var sth = new CancelableTaskRunner(false);
        var i = 0;
        var sw = Stopwatch.StartNew();
        var t1 = sth.RunAndCancelPrevious<int?>(async x =>
        {
            await Task.Delay(500, x);
            if (!x.IsCancellationRequested) { Assert.Equal(0, i++); return 1; }
            return null;
        });
        var t2 = sth.RunAndCancelPrevious<int?>(async x =>
        {
            await Task.Delay(1000, x);
            if (!x.IsCancellationRequested) { Assert.Equal(0, i++); return 2; }
            return null;
        });
        var t3 = sth.RunAndCancelPrevious<int?>(async x =>
        {
            await Task.Delay(250, x);
            if (!x.IsCancellationRequested) { Assert.Equal(0, i++); return 3; }
            return null;
        });
        await t3;
        Assert.True(Math.Abs(sw.ElapsedMilliseconds - 250) < 50);
        await Assert.ThrowsAsync<TaskCanceledException>(() => Task.WhenAll(t1, t2, t3));
        Assert.True(t1.IsCanceled);
        Assert.True(t2.IsCanceled);
        Assert.True(t3.IsCompleted);
    }

    [Fact]
    public async Task CancelableTasks2Test()
    {
        var sth = new CancelableTaskRunner(false);
        var i = 0;
        var sw = Stopwatch.StartNew();
        var t1 = sth.RunAndCancelPrevious<int?>(async x =>
        {
            await Task.Delay(500, x);
            if (!x.IsCancellationRequested) { Assert.Equal(0, i++); return 1; }
            return null;
        });
        await Task.Delay(750);
        var t2 = sth.RunAndCancelPrevious<int?>(async x =>
        {
            await Task.Delay(1000, x);
            if (!x.IsCancellationRequested) { Assert.Equal(1, i++); return 2; }
            return null;
        });
        var t3 = sth.RunAndCancelPrevious<int?>(async x =>
        {
            await Task.Delay(250, x);
            if (!x.IsCancellationRequested) { Assert.Equal(1, i++); return 3; }
            return null;
        });
        await t3;
        Assert.True(Math.Abs(sw.ElapsedMilliseconds - 1000) < 50);
        await Assert.ThrowsAsync<TaskCanceledException>(() => Task.WhenAll(t1, t2, t3));
        Assert.True(t1.IsCompleted);
        Assert.True(t2.IsCanceled);
        Assert.True(t3.IsCompleted);
    }
}
