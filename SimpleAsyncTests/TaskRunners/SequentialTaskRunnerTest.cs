﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Xunit;

namespace Tocronx.SimpleAsync.Tests.TaskRunners;

public class SequentialTaskRunnerTest
{
    [Fact]
    public async Task EnqueueIfEmptyTest()
    {
        var sth = new SequentialTaskRunner();
        Assert.True(sth.IsEmpty);
        Assert.True(sth.IsIdle);

        var taskCompletionSource1 = new TaskCompletionSource<bool>();
        var task1 = sth.EnqueueIfEmpty(() => taskCompletionSource1.Task);
        Assert.True(sth.IsEmpty);
        Assert.False(sth.IsIdle);

        var taskCompletionSource2 = new TaskCompletionSource<bool>();
        var task2 = sth.EnqueueIfEmpty(() => taskCompletionSource2.Task);
        Assert.False(sth.IsEmpty);
        Assert.False(sth.IsIdle);

        var task3 = sth.EnqueueIfEmpty(() => Task.Delay(10));
        Assert.True(task3.IsCompleted);
        Assert.False(sth.IsEmpty);
        Assert.False(sth.IsIdle);

        taskCompletionSource1.SetResult(true);
        Assert.True(sth.IsEmpty);
        Assert.False(sth.IsIdle);

        taskCompletionSource2.SetResult(true);
        Assert.True(sth.IsEmpty);
        Assert.True(sth.IsIdle);

        await Task.WhenAll(task1, task2, task3);
    }

    [Fact]
    public async Task EnqueueTest()
    {
        var sth = new SequentialTaskRunner();
        Assert.True(sth.IsEmpty);
        Assert.True(sth.IsIdle);

        var taskCompletionSource1 = new TaskCompletionSource<bool>();
        var task1 = sth.Enqueue(async () => await taskCompletionSource1.Task);
        Assert.True(sth.IsEmpty);
        Assert.False(sth.IsIdle);

        var taskCompletionSource2 = new TaskCompletionSource<bool>();
        var task2 = sth.Enqueue(async () => await taskCompletionSource2.Task);
        Assert.False(sth.IsEmpty);
        Assert.False(sth.IsIdle);

        var taskCompletionSource3 = new TaskCompletionSource<bool>();
        var task3 = sth.Enqueue(async () => await taskCompletionSource3.Task);
        Assert.False(sth.IsEmpty);
        Assert.False(sth.IsIdle);

        taskCompletionSource1.SetResult(true);
        Assert.False(sth.IsEmpty);
        Assert.False(sth.IsIdle);

        taskCompletionSource2.SetResult(true);
        Assert.True(sth.IsEmpty);
        Assert.False(sth.IsIdle);

        taskCompletionSource3.SetResult(true);
        Assert.True(sth.IsEmpty);
        Assert.True(sth.IsIdle);

        await Task.WhenAll(task1, task2, task3);
    }

    [Fact]
    public async Task EnqueueWithReturnValueTest()
    {
        var sth = new SequentialTaskRunner();
        Assert.True(sth.IsEmpty);
        Assert.True(sth.IsIdle);

        var taskCompletionSource1 = new TaskCompletionSource<int>();
        var task1 = sth.Enqueue(async () => await taskCompletionSource1.Task);
        Assert.True(sth.IsEmpty);
        Assert.False(sth.IsIdle);

        var taskCompletionSource2 = new TaskCompletionSource<int>();
        var task2 = sth.Enqueue(async () => await taskCompletionSource2.Task);
        Assert.False(sth.IsEmpty);
        Assert.False(sth.IsIdle);

        var taskCompletionSource3 = new TaskCompletionSource<int>();
        var task3 = sth.Enqueue(async () => await taskCompletionSource3.Task);
        Assert.False(sth.IsEmpty);
        Assert.False(sth.IsIdle);

        taskCompletionSource1.SetResult(5);
        Assert.Equal(5, await task1);
        Assert.False(sth.IsEmpty);
        Assert.False(sth.IsIdle);

        taskCompletionSource2.SetResult(10);
        Assert.Equal(10, await task2);
        Assert.True(sth.IsEmpty);
        Assert.False(sth.IsIdle);

        taskCompletionSource3.SetResult(15);
        Assert.Equal(15, await task3);
        Assert.True(sth.IsEmpty);
        Assert.True(sth.IsIdle);
    }

    [Fact]
    public async Task SequentialTasksTest()
    {
        var sth = new SequentialTaskRunner();
        var i = 0;
        var sw = Stopwatch.StartNew();
        var t1 = sth.Enqueue(async () =>
        {
            await Task.Delay(500);
            Assert.Equal(0, i++);
        });
        var t2 = sth.Enqueue(async () =>
        {
            await Task.Delay(1000);
            Assert.Equal(1, i++);
        });
        var t3 = sth.Enqueue(async () =>
        {
            await Task.Delay(250);
            Assert.Equal(2, i++);
        });
        await sth.ExecutionTask;
        Assert.True(t1.IsCompleted, "t1 must be completed!");
        Assert.True(t2.IsCompleted, "t2 must be completed!");
        Assert.True(t3.IsCompleted, "t3 must be completed!");
        Assert.True(sth.IsIdle, "SequentialTaskRunner should now be empty!");
        Assert.True(Math.Abs(sw.ElapsedMilliseconds - 1750) < 100, $"Difference to expected execution time to big: {Math.Abs(sw.ElapsedMilliseconds - 1750)}ms");
    }

    [Fact]
    public async Task SequentialDataTasksTest()
    {
        var sth = new SequentialTaskRunner();
        var i = 0;
        var sw = Stopwatch.StartNew();
        var t1 = sth.Enqueue(async () =>
        {
            await Task.Delay(500);
            return ++i;
        });
        var t1c = t1.ContinueWith(x => Assert.Equal(1, x.Result));
        var t2 = sth.Enqueue(async () =>
        {
            await Task.Delay(1000);
            return ++i;
        });
        var t2c = t2.ContinueWith(x => Assert.Equal(2, x.Result));
        var t3 = sth.Enqueue(async () =>
        {
            await Task.Delay(250);
            return ++i;
        });
        var t3c = t3.ContinueWith(x => Assert.Equal(3, x.Result));
        await sth.ExecutionTask;
        Assert.True(t1.IsCompleted, "t1 must be completed!");
        Assert.True(t2.IsCompleted, "t2 must be completed!");
        Assert.True(t3.IsCompleted, "t3 must be completed!");
        Assert.True(sth.IsIdle, "SequentialTaskRunner should now be empty!");
        Assert.True(Math.Abs(sw.ElapsedMilliseconds - 1750) < 100, $"Difference to expected execution time to big: {Math.Abs(sw.ElapsedMilliseconds - 1750)}ms");
        await Task.WhenAll(t1c, t2c, t3c);
    }

}
