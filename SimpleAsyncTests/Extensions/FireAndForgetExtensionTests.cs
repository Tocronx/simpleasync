﻿using System;
using System.Threading.Tasks;
using Xunit;

namespace Tocronx.SimpleAsync.Tests.Extensions;


public class FireAndForgetExtensionTests
{
    private readonly Exception exception = new("Test Exception");
    private readonly TaskCompletionSource tsc = new();

    [Fact]
    public async Task FireAndForget_ShouldCallDefaultHandler()
    {
        Exception? thrownException = null;
        FireAndForgetExtension.OnException = ex =>
        {
            thrownException = ex;
            tsc.SetResult();
        };

        Task.Run(() => throw exception).FireAndForget();
        await tsc.Task.WaitAsync(TimeSpan.FromMilliseconds(500));

        Assert.Equal(exception, thrownException);
    }

    [Fact]
    public async Task FireAndForget_WithOnException_ShouldExecuteOnException()
    {
        Exception? thrownException = null;
        void onException(Exception ex)
        {
            thrownException = ex;
            tsc.SetResult();
        }

        Task.Run(() => throw exception).FireAndForget(onException);
        await tsc.Task.WaitAsync(TimeSpan.FromMilliseconds(500));

        Assert.Equal(exception, thrownException);
    }

    [Fact]
    public async Task FireAndForget_WithOnExceptionReturningFalse_ShouldCallDefaultHandler()
    {
        Exception? thrownException = null;
        FireAndForgetExtension.OnException = ex =>
        {
            thrownException = ex;
            tsc.SetResult();
        };

        Task.Run(() => throw exception).FireAndForget(x => false);
        await tsc.Task.WaitAsync(TimeSpan.FromMilliseconds(500));

        Assert.Equal(exception, thrownException);
    }


    [Fact]
    public async Task InterceptException_WithOnExceptionReturningFalse_ShouldThrowException()
    {
        Exception? thrownException = null;
        bool onException(Exception ex)
        {
            thrownException = ex;
            return false;
        }

        await Assert.ThrowsAsync<Exception>(() => Task.Run(() => throw exception).InterceptException<Exception>(onException));

        Assert.Equal(exception, thrownException);
    }

    [Fact]
    public async Task InterceptException_WithOnExceptionReturningTrue_ShouldNotThrowException()
    {
        Exception? thrownException = null;
        bool onException(Exception ex)
        {
            thrownException = ex;
            return true;
        }

        await Task.Run(() => throw exception).InterceptException<Exception>(onException);

        Assert.Equal(exception, thrownException);
    }
}
