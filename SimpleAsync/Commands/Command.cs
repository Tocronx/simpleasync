﻿using System;

namespace Tocronx.SimpleAsync;

/// <summary>
/// Parameterless ICommand implementation.
/// </summary>
public class Command : Command<object>
{
    /// <summary>Constructor</summary>
    /// <param name="action">Action that is executed when the command is executed.</param>
    public Command(Action action) : base(x => action()) { }
}

/// <summary>
/// Generic ICommand implementation.
/// </summary>
/// <typeparam name="T"></typeparam>
public class Command<T> : CommandBase
{
    private readonly Action<T?> action;

    /// <summary>Constructor</summary>
    /// <param name="action">Action that is executed when the command is executed.</param>
    public Command(Action<T?> action) => this.action = action;

    /// <summary>
    /// Defines the method to be called when the command is invoked.
    /// </summary>
    /// <param name="parameter">Data used by the command. If the command does not require data to be passed, this object can be set to null.</param>
    public override void Execute(object? parameter)
    {
        if (parameter is not null && parameter is not T)
        {
            throw new ArgumentException($"The argument {nameof(parameter)} must be of type {typeof(T).FullName}.", nameof(parameter));
        }
        if (IsExecutable)
        {
            try
            {
                BeginExecute();
                T? value = (T?)parameter;
                action(value);
            }
            finally
            {
                EndExecute();
            }
        }
    }

}

/// <summary>
/// Generic ICommand implementation that does not allow null as command parameter
/// </summary>
public class NullCheckingCommand<T> : Command<T> where T : notnull
{
    /// <summary>Constructor</summary>
    /// <param name="action">Action that is executed when the command is executed.</param>
    public NullCheckingCommand(Action<T> action)
        : base(x => action(x ?? throw new NullReferenceException($"The command parameter must be not null and of type {typeof(T).FullName}!")))
    {
    }
}
