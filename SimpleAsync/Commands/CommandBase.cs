﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows.Input;

namespace Tocronx.SimpleAsync;

/// <summary>
/// Base class for ICommand implementations.
/// </summary>
public abstract class CommandBase : ICommand, INotifyPropertyChanged
{
    private bool isExecutable = true;
    private bool canExecuteParallel;
    private volatile int isRunningCount;
    private INotifyPropertyChanged? bindingObject;
    private string? bindingPropertyName;
    private Func<bool>? bindingFunction;

    /// <summary>
    /// PropertyChanged event.
    /// </summary>
    public event PropertyChangedEventHandler? PropertyChanged;

    /// <summary>
    /// CanExecuteChanged event.
    /// </summary>
    public event EventHandler? CanExecuteChanged;

    /// <summary>
    /// Property indicating if the command can be executed or not.
    /// </summary>
    public bool IsExecutable { get => isExecutable && (isRunningCount == 0 || canExecuteParallel); set => Set(ref isExecutable, value); }

    /// <summary>
    /// Property to define if the command can be executed in parallel.
    /// </summary>
    public bool CanExecuteParallel { get => canExecuteParallel; set => Set(ref canExecuteParallel, value); }

    /// <summary>
    /// Defines the method that determines whether the command can execute in its current state.
    /// </summary>
    /// <param name="parameter">Data used by the command. If the command does not require data to be passed, this object can be set to null.</param>
    /// <returns>True if this command can be executed; otherwise, false.</returns>
    public bool CanExecute(object? parameter) => IsExecutable;

    /// <summary>
    /// Defines the method to be called when the command is invoked.
    /// </summary>
    /// <param name="parameter">Data used by the command. If the command does not require data to be passed, this object can be set to null.</param>
    public abstract void Execute(object? parameter);

    /// <summary>
    /// Binds a property to the can execute state of the command.
    /// </summary>
    /// <param name="canExecuteBinding">Expression of type () => INotifyPropertyChangedInstance.Property</param>
    public void SetCanExecuteBinding(Expression<Func<bool>> canExecuteBinding)
    {
        if (bindingObject != null) { bindingObject.PropertyChanged -= BindingObject_PropertyChanged; }
        if (canExecuteBinding == null) { return; }
        var body = canExecuteBinding.Body as MemberExpression;
        var memberExpression = body?.Expression as MemberExpression;
        var constantExpression = memberExpression?.Expression as ConstantExpression;
        var fieldInfo = memberExpression?.Member as FieldInfo;
        bindingObject = fieldInfo?.GetValue(constantExpression?.Value) as INotifyPropertyChanged;
        bindingPropertyName = body?.Member?.Name;
        if (bindingObject == null || bindingPropertyName == null)
        {
            throw new ArgumentException($"Valid expression of type () => INotifyPropertyChangedInstance.Property required!", nameof(canExecuteBinding));
        }
        bindingFunction = canExecuteBinding.Compile();
        bindingObject.PropertyChanged += BindingObject_PropertyChanged;
        BindingObject_PropertyChanged(bindingObject, new PropertyChangedEventArgs(bindingPropertyName));
    }

    /// <summary>
    /// Method to inform the CommandBase class that execution of the command begins.
    /// </summary>
    protected void BeginExecute()
    {
        Interlocked.Increment(ref isRunningCount);
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsExecutable)));
        CanExecuteChanged?.Invoke(this, EventArgs.Empty);
    }

    /// <summary>
    /// Method to inform the CommandBase class that execution of the command ends.
    /// </summary>
    protected void EndExecute()
    {
        Interlocked.Decrement(ref isRunningCount);
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsExecutable)));
        CanExecuteChanged?.Invoke(this, EventArgs.Empty);
    }

    private void BindingObject_PropertyChanged(object? sender, PropertyChangedEventArgs e)
    {
        if (bindingFunction is not null && e.PropertyName == bindingPropertyName)
        {
            IsExecutable = bindingFunction.Invoke();
        }
    }

    private void Set<TValue>(ref TValue value, TValue newValue, [CallerMemberName] string propertyName = "")
    {
        if (Equals(value, newValue)) { return; }
        value = newValue;
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        if (propertyName != nameof(IsExecutable))
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsExecutable)));
        }
        CanExecuteChanged?.Invoke(this, EventArgs.Empty);
    }

}
