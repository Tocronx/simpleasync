﻿using System;
using System.Threading.Tasks;

namespace Tocronx.SimpleAsync;

/// <summary>
/// Parameterless async ICommand implementation.
/// </summary>
public class AsyncCommand : AsyncCommand<object>
{
    /// <summary>Constructor</summary>
    /// <param name="action">Action that is executed when the command is executed.</param>
    public AsyncCommand(Func<Task> action) : base(x => action()) { }
}

/// <summary>
/// Generic async ICommand implementation.
/// </summary>
public class AsyncCommand<T> : CommandBase
{
    private readonly Func<T?, Task> action;

    /// <summary>Constructor</summary>
    /// <param name="action">Action that is executed when the command is executed.</param>
    public AsyncCommand(Func<T?, Task> action) => this.action = action;

    /// <summary>
    /// Defines the method to be called when the command is invoked.
    /// </summary>
    /// <param name="parameter">Data used by the command. If the command does not require data to be passed, this object can be set to null.</param>
    public async override void Execute(object? parameter)
    {
        if (parameter is not null && parameter is not T)
        {
            throw new ArgumentException($"The argument {nameof(parameter)} must be of type {typeof(T).FullName}.", nameof(parameter));
        }
        await ExecuteAsync((T?)parameter);
    }

    /// <summary>
    /// Defines the async method to be called when the command is invoked.
    /// </summary>
    /// <param name="parameter">Data used by the command. If the command does not require data to be passed, this object can be set to null.</param>
    /// <returns> A task that represents the asynchronous command operation.</returns>
    public async Task ExecuteAsync(T? parameter)
    {
        if (IsExecutable)
        {
            try
            {
                BeginExecute();
                await action(parameter);
            }
            finally
            {
                EndExecute();
            }
        }
    }

}

/// <summary>
/// Generic async ICommand implementation that not allows null as command parameter.
/// </summary>
public class AsyncNullCheckingCommand<T> : AsyncCommand<T> where T : notnull
{
    /// <summary>Constructor</summary>
    /// <param name="action">Action that is executed when the command is executed.</param>
    public AsyncNullCheckingCommand(Func<T, Task> action)
        : base(x => action(x ?? throw new NullReferenceException($"The command parameter must be not null and of type {typeof(T).FullName}!")))
    {
    }
}
