﻿using System;
using System.Threading.Tasks;

namespace Tocronx.SimpleAsync;

/// <summary>
/// Simple utility class for retrying asynchronous operations.
/// </summary>
public static class RetryHelper
{
    /// <summary>
    /// Retries executing a Task until it either fails the maximum number of attempts specified by maxRetries or succeeds.
    /// </summary>
    /// <typeparam name="TResult">The result type of the Task.</typeparam>
    /// <param name="taskFactoryFunction">A function that creates the Task to be retried.</param>
    /// <param name="maxRetries">The maximum number of attempts to retry the Task.</param>
    /// <param name="delay">The delay between each retry attempt.</param>
    /// <returns>The result of the Task.</returns>
    public static async Task<TResult> Retry<TResult>(Func<Task<TResult>> taskFactoryFunction, int maxRetries, TimeSpan delay)
    {
        for (int i = 0; i < maxRetries; i++)
        {
            try
            {
                return await taskFactoryFunction().ConfigureAwait(false);
            }
            catch
            {
                if (i == maxRetries - 1)
                {
                    throw;
                }
                await Task.Delay(delay).ConfigureAwait(false);
            }
        }
        return default!;
    }

}
