﻿using System;
using System.Threading.Tasks;

namespace Tocronx.SimpleAsync;

/// <summary>
/// Simple utility class to call asynchronous functions synchronously.
/// </summary>
public static class AsyncHelper
{
    /// <summary>
    /// Helper function to execute an Async function synchronously.
    /// </summary>
    /// <param name="func">Async function.</param>
    public static void RunSync(Func<Task> func) => Task.Run(func).GetAwaiter().GetResult();

    /// <summary>
    /// Helper function to execute an Async function synchronously.
    /// </summary>
    /// <typeparam name="T">Type of the async functions result.</typeparam>
    /// <param name="func">Async function.</param>
    /// <returns>Result of the async function</returns>
    public static T RunSync<T>(Func<Task<T>> func) => Task.Run(func).GetAwaiter().GetResult();
}
