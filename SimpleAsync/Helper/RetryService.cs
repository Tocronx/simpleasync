﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Tocronx.SimpleAsync;

/// <summary>
/// Provides a service for retrying asynchronous tasks.
/// </summary>
public class RetryService
{
    /// <summary>
    /// Gets or sets the default maximum number of retries.
    /// </summary>
    public int DefaultMaxRetries { get; set; } = 1;

    /// <summary>
    /// Gets or sets the function used to calculate the delay between retries.
    /// </summary>
    public Func<int, TimeSpan> RetryDelayFunction { get; set; } = attempt => TimeSpan.FromSeconds(attempt * 2);

    /// <summary>
    /// Gets or sets the error handler to handle exceptions from failed attempts.
    /// </summary>
    public Action<int, Exception>? ErrorHandler { get; set; }

    /// <summary>
    /// Executes a given asynchronous task with retries.
    /// </summary>
    /// <typeparam name="TResult">The type of the result.</typeparam>
    /// <param name="taskFactoryFunction">The asynchronous task factory function.</param>
    /// <param name="cancellationToken">A optional cancellation token.</param>
    /// <returns>The result of the asynchronous operation.</returns>
    public async Task<TResult> Retry<TResult>(Func<CancellationToken, Task<TResult>> taskFactoryFunction, CancellationToken cancellationToken = default)
    {
        var maxRetries = DefaultMaxRetries;
        for (int attempt = 0; attempt <= maxRetries; attempt++)
        {
            try
            {
                return await taskFactoryFunction(cancellationToken).ConfigureAwait(false);
            }
            catch (TaskCanceledException) when (cancellationToken.IsCancellationRequested)
            {
                throw;
            }
            catch (Exception e)
            {
                ErrorHandler?.Invoke(attempt, e);
                if (attempt == maxRetries) { throw; }
                await Task.Delay(RetryDelayFunction(attempt), cancellationToken).ConfigureAwait(false);
            }
        }
#if NET7_0_OR_GREATER
        throw new UnreachableException();
#else
        throw new InvalidOperationException("The program executed an instruction that was thought to be unreachable.");
#endif
    }

}
