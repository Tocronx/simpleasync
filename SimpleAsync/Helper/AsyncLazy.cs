﻿using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace Tocronx.SimpleAsync;

/// <summary>
/// AsyncLazy class providing thread safe support for asynchronous lazy initialization.
/// </summary>
/// <typeparam name="T">The type of object that is being asynchronously initialized.</typeparam>
public class AsyncLazy<T> : Lazy<Task<T>>
{
    /// <summary>
    /// Initializes a new instance of the <see cref="AsyncLazy{T}"/> class.
    /// </summary>
    /// <param name="valueFactory">The function that is invoked to produce the result value.</param>
    public AsyncLazy(Func<T> valueFactory) :
        base(() => Task.Run(valueFactory))
    { }

    /// <summary>
    /// Initializes a new instance of the <see cref="AsyncLazy{T}"/> class.
    /// </summary>
    /// <param name="taskFactory">The asynchronous function that is invoked to produce the result value.</param>
    public AsyncLazy(Func<Task<T>> taskFactory) :
        this(taskFactory, false)
    { }

    /// <summary>
    /// Initializes a new instance of the <see cref="AsyncLazy{T}"/> class.
    /// </summary>
    /// <param name="taskFactory">The asynchronous function that is invoked to produce the result value.</param>
    /// <param name="runOnThreadPool">A bool indicating to start the task on a thread pool thread.</param>
    public AsyncLazy(Func<Task<T>> taskFactory, bool runOnThreadPool) :
        base(runOnThreadPool ? () => Task.Run(() => taskFactory()) : taskFactory)
    { }

    /// <summary>
    /// Gets the task awaiter for the value of this instance.
    /// </summary>
    /// <returns>The task awaiter for the value of this instance.</returns>
    public TaskAwaiter<T> GetAwaiter() => Value.GetAwaiter();
}
