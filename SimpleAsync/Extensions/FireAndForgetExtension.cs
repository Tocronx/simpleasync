﻿using System;
using System.Threading.Tasks;

namespace Tocronx.SimpleAsync;


/// <summary>
/// Static class for the fire and forget extension for task objects.
/// </summary>
public static class FireAndForgetExtension
{
    /// <summary>
    /// Property to set an global exception handler
    /// </summary>
    public static Action<Exception>? OnException { get; set; }


    /// <summary>
    /// Safely execute the Task without waiting for it to complete before moving to the next line of code.
    /// </summary>
    /// <param name="task">Task.</param>
    /// <param name="onException">If an exception is thrown in the Task onException will execute. If onException is null, the exception will be re-thrown.</param>
    /// <param name="continueOnCapturedContext">True to marshal the continuation back to the original context captured otherwise false.</param>
    public static void FireAndForget(this Task task, Action<Exception>? onException, bool continueOnCapturedContext = false)
    {
        if (onException is not null)
        {
            task = task.InterceptException<Exception>(e => { onException(e); return true; }, continueOnCapturedContext);
        }
        task.FireAndForget(continueOnCapturedContext);
    }

    /// <summary>
    /// Safely execute the Task without waiting for it to complete before moving to the next line of code.
    /// </summary>
    /// <param name="task">Task.</param>
    /// <param name="continueOnCapturedContext">True to marshal the continuation back to the original context captured otherwise false.</param>
    public static async void FireAndForget(this Task task, bool continueOnCapturedContext = false)
    {
        try
        {
            await task.ConfigureAwait(continueOnCapturedContext);
        }
        catch (Exception ex)
        {
            OnException?.Invoke(ex);
        }
    }

    /// <summary>
    /// Safely execute the Task without waiting for it to complete before moving to the next line of code.
    /// </summary>
    /// <param name="task">Task.</param>
    /// <param name="onException">If an exception is thrown the Task onException will execute. If onException return false, the default exception handler will be called too.</param>
    /// <param name="continueOnCapturedContext">True to marshal the continuation back to the original context captured otherwise false.</param>
    public static async void FireAndForget(this Task task, Func<Exception, bool> onException, bool continueOnCapturedContext = false)
    {
        try
        {
            await task.InterceptException(onException, continueOnCapturedContext).ConfigureAwait(continueOnCapturedContext);
        }
        catch (Exception ex)
        {
            OnException?.Invoke(ex);
        }
    }

    /// <summary>
    /// Intercepts an exception of type T and executes the onException function if the exception is thrown.
    /// </summary>
    /// <typeparam name="T">The type of exception to intercept.</typeparam>
    /// <param name="task">The task to intercept exceptions for.</param>
    /// <param name="onException">The function to execute if an exception of type T is thrown. If false is returned the exceptions is rethrown.</param>
    /// <param name="continueOnCapturedContext">True to marshal the continuation back to the original context captured otherwise false.</param>
    public static async Task InterceptException<T>(this Task task, Func<T, bool> onException, bool continueOnCapturedContext = false)
        where T : Exception
    {
        try
        {
            await task.ConfigureAwait(continueOnCapturedContext);
        }
        catch (T ex)
        {
            if (!onException(ex))
            {
                throw;
            }
        }
    }

}
