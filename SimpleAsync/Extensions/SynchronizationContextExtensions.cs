﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Tocronx.SimpleAsync.SynchronizationContextExtensions;

/// <summary>
/// Provides extension methods for <see cref="SynchronizationContext"/>.
/// </summary>
public static class SynchronizationContextExtensions
{
    /// <summary>
    /// Dispatches an action to the synchronization context if it is not null and not the current synchronization context.
    /// </summary>
    /// <param name="synchronizationContext">The synchronization context to dispatch the action to.</param>
    /// <param name="action">The action to dispatch.</param>
    public static void Dispatch(this SynchronizationContext? synchronizationContext, Action action)
    {
        if (synchronizationContext is null || SynchronizationContext.Current == synchronizationContext)
        {
            action();
            return;
        }
        synchronizationContext.Send(x => action(), null);
    }

    /// <summary>
    /// Asynchronously dispatches an action to the synchronization context if it is not null and not the current synchronization context.
    /// </summary>
    /// <param name="synchronizationContext">The synchronization context to dispatch the action to.</param>
    /// <param name="action">The action to dispatch.</param>
    /// <returns>A task that represents the asynchronous operation.</returns>
    public static Task DispatchAsync(this SynchronizationContext? synchronizationContext, Action action)
    {
        if (synchronizationContext is null || SynchronizationContext.Current == synchronizationContext)
        {
            action();
            return Task.CompletedTask;
        }
        var tcs = new TaskCompletionSource<bool>();
        synchronizationContext.Post(x =>
        {
            try
            {
                action();
                tcs.SetResult(true);
            }
            catch (Exception e) { tcs.SetException(e); }
        }, null);
        return tcs.Task;
    }

}
