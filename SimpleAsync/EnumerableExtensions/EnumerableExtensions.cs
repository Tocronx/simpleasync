﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace Tocronx.SimpleAsync.EnumerableExtensions;

/// <summary>
/// Asynchronous extension methods for enumerables.
/// </summary>
public static class EnumerableExtensions
{
    /// <summary>
    /// Executes a async function for every item limiting the number of parallel running tasks.
    /// </summary>
    /// <param name="items">The enumerable.</param>
    /// <param name="function">The function to execute for every item.</param>
    /// <param name="maxParallelTasks">The max number of parallel running tasks.</param>
    /// <returns>Task with a enumerable set of values generated from the items by the function.</returns>
    [Obsolete("Use ParallelSelect instead.")]
    public static Task<IEnumerable<TResult>> ForEachAsync<T, TResult>(this IEnumerable<T> items, Func<T, Task<TResult>> function, int maxParallelTasks)
    {
        return items.ParallelSelect(function, maxParallelTasks);
    }

    /// <summary>
    /// Executes a async function for every item and returns the tasks.
    /// With max preloading can be specified for how many items the async function execution is started before the current item is returned.
    /// </summary>
    /// <param name="items">The enumerable.</param>
    /// <param name="function">The function to execute for every item.</param>
    /// <param name="maxPreloading">The maximum number of items for which the function is applied in advance.</param>
    /// <returns>Enumerable set of Tasks generated from the items by the function.</returns>
#if NET6_0_OR_GREATER
    [Obsolete("Use ParallelSelectAsync instead.")]
#endif
    public static IEnumerable<Task<TResult>> ToPreloadingEnumerable<T, TResult>(this IEnumerable<T> items, Func<T, Task<TResult>> function, int maxPreloading)
    {
        maxPreloading = Math.Max(maxPreloading, 1);
        var taskQueue = new Queue<Task<TResult>>();
        foreach (var item in items)
        {
            if (taskQueue.Count >= maxPreloading)
            {
                yield return taskQueue.Dequeue();
            }
            taskQueue.Enqueue(function(item));
        }
        while (taskQueue.Count > 0)
        {
            yield return taskQueue.Dequeue();
        }
    }

    /// <summary>
    /// Executes a action for every item in parallel limiting the number of parallel running tasks.
    /// </summary>
    /// <param name="items">The enumerable.</param>
    /// <param name="action">The action to execute for every item.</param>
    /// <param name="maxParallelTasks">The max number of parallel running tasks.</param>
    /// <returns>Task for awaiting the parallel execution.</returns>
    public static async Task ParallelForEach<T>(this IEnumerable<T> items, Action<T> action, int maxParallelTasks = 4)
    {
        var queue = new ConcurrentQueue<T>(items);
        maxParallelTasks = Math.Max(maxParallelTasks, 1);
        var tasks = new List<Task>();
        for (var i = 0; i < maxParallelTasks; i++)
        {
            tasks.Add(Task.Run(() =>
            {
                while (queue.TryDequeue(out var value))
                {
                    action(value);
                }
            }));
        }
        await Task.WhenAll(tasks);
    }

    /// <summary>
    /// Executes an asynchronous function for every item in parallel limiting the number of parallel running tasks.
    /// </summary>
    /// <typeparam name="T">The type of the items in the enumerable.</typeparam>
    /// <param name="items">The enumerable.</param>
    /// <param name="function">The asynchronous function to execute for every item.</param>
    /// <param name="maxParallelTasks">The maximum number of parallel running tasks. If null, the number of tasks will be equal to the number of processors on the machine.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken"/> that can be used to cancel the operation.</param>
    /// <returns>A <see cref="Task"/> representing the parallel execution.</returns>
    public static async Task ParallelForEach<T>(this IEnumerable<T> items, Func<T, Task> function, int? maxParallelTasks = null, CancellationToken cancellationToken = default)
    {
        if (items is null) { throw new ArgumentNullException(nameof(items)); }
        if (function is null) { throw new ArgumentNullException(nameof(function)); }
        SemaphoreSlim semaphore = new(Math.Max(maxParallelTasks ?? Environment.ProcessorCount, 1));
        List<Task> list = new();
        foreach (var item in items)
        {
            await semaphore.WaitAsync(cancellationToken).ConfigureAwait(false);
            list.Add(Run(item));
        }
        await Task.WhenAll(list).ConfigureAwait(false);
        async Task Run(T item)
        {
            await function(item).ConfigureAwait(false);
            semaphore.Release();
        }
    }

    /// <summary>
    /// Asynchronously projects each element of an <see cref="IEnumerable{T}"/> into a new form, allowing multiple parallel tasks.
    /// </summary>
    /// <typeparam name="T">The type of the elements in the source enumerable.</typeparam>
    /// <typeparam name="TResult">The type of the value returned by <paramref name="function"/>.</typeparam>
    /// <param name="items">The source <see cref="IEnumerable{T}"/>.</param>
    /// <param name="function">A function to apply to each element.</param>
    /// <param name="maxParallelTasks">The maximum number of parallel tasks to use. If null, the number of tasks will be equal to the number of processors on the machine.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken"/> that can be used to cancel the operation.</param>
    /// <returns>An <see cref="Task"/> with a <see cref="IEnumerable{TResult}"/> whose elements are the result of invoking the transform function on each element of <paramref name="items"/>.</returns>
    public static async Task<IEnumerable<TResult>> ParallelSelect<T, TResult>(this IEnumerable<T> items, Func<T, Task<TResult>> function,
        int? maxParallelTasks = null, CancellationToken cancellationToken = default)
    {
        if (items is null) { throw new ArgumentNullException(nameof(items)); }
        if (function is null) { throw new ArgumentNullException(nameof(function)); }
        maxParallelTasks = Math.Max(maxParallelTasks ?? Environment.ProcessorCount, 0);
        List<Task<TResult>> list = new();
        foreach (var item in items)
        {
            cancellationToken.ThrowIfCancellationRequested();
            var currentTasks = list.Where(x => !x.IsCompleted).ToList();
            if (currentTasks.Count > maxParallelTasks)
            {
                await Task.WhenAny(currentTasks).ConfigureAwait(false);
            }
            list.Add(function(item));
        }
        await Task.WhenAll(list).ConfigureAwait(false);
        return list.Select(x => x.GetAwaiter().GetResult());
    }

#if NET6_0_OR_GREATER
    /// <summary>
    /// Asynchronously projects each element of an <see cref="IEnumerable{T}"/> into a new form, allowing multiple parallel tasks.
    /// </summary>
    /// <typeparam name="T">The type of the elements in the source enumerable.</typeparam>
    /// <typeparam name="TResult">The type of the value returned by <paramref name="function"/>.</typeparam>
    /// <param name="items">The source <see cref="IEnumerable{T}"/>.</param>
    /// <param name="function">A function to apply to each element.</param>
    /// <param name="maxParallelTasks">The maximum number of parallel tasks to use. If null, the number of tasks will be equal to the number of processors on the machine.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken"/> that can be used to cancel the operation.</param>
    /// <returns>An <see cref="IAsyncEnumerable{TResult}"/> whose elements are the result of invoking the transform function on each element of <paramref name="items"/>.</returns>
    public static async IAsyncEnumerable<TResult> ParallelSelectAsync<T, TResult>(this IEnumerable<T> items, Func<T, Task<TResult>> function,
        int? maxParallelTasks = null, [EnumeratorCancellation] CancellationToken cancellationToken = default)
    {
        ArgumentNullException.ThrowIfNull(items);
        ArgumentNullException.ThrowIfNull(function);
        maxParallelTasks = Math.Max(maxParallelTasks ?? Environment.ProcessorCount, 0);
        Queue<Task<TResult>> taskQueue = new();
        foreach (var item in items)
        {
            cancellationToken.ThrowIfCancellationRequested();
            var currentTasks = taskQueue.Where(x => !x.IsCompleted).ToList();
            if (currentTasks.Count > maxParallelTasks)
            {
                await Task.WhenAny(currentTasks).ConfigureAwait(false);
            }
            taskQueue.Enqueue(function(item));
            while (taskQueue.TryPeek(out var task) && task.IsCompleted)
            {
                yield return await taskQueue.Dequeue().ConfigureAwait(false);
            }
        }
        while (taskQueue.Count > 0)
        {
            yield return await taskQueue.Dequeue().ConfigureAwait(false);
        }
    }
#endif
}
