﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace Tocronx.SimpleAsync.EnumerableExtensions;

#if NET6_0_OR_GREATER
/// <summary>
/// Provides extension methods for working with asynchronous enumerables.
/// </summary>
public static class AsyncEnumerableExtensions
{

    /// <summary>
    /// Asynchronously converts an <see cref="IAsyncEnumerable{T}"/> to a <see cref="List{T}"/>.
    /// </summary>
    /// <typeparam name="T">The type of the elements in the enumerable.</typeparam>
    /// <param name="source">The source <see cref="IAsyncEnumerable{T}"/>.</param>
    /// <returns>A <see cref="Task{TResult}"/> representing the asynchronous operation. The result of the task is a <see cref="List{T}"/> containing the elements of the enumerable.</returns>
    public static async Task<List<T>> ToListAsync<T>(this IAsyncEnumerable<T> source)
    {
        List<T> list = [];
        await foreach (var item in source)
        {
            list.Add(item);
        }
        return list;
    }

    /// <summary>
    /// Asynchronously converts an <see cref="IAsyncEnumerable{T}"/> to a <see cref="HashSet{T}"/>.
    /// </summary>
    /// <typeparam name="T">The type of the elements in the enumerable.</typeparam>
    /// <param name="source">The source <see cref="IAsyncEnumerable{T}"/>.</param>
    /// <returns>A <see cref="Task{TResult}"/> representing the asynchronous operation. The result of the task is a <see cref="HashSet{T}"/> containing the elements of the enumerable.</returns>
    public static async Task<HashSet<T>> ToHashSetAsync<T>(this IAsyncEnumerable<T> source)
    {
        HashSet<T> hashSet = [];
        await foreach (var item in source)
        {
            hashSet.Add(item);
        }
        return hashSet;
    }

    /// <summary>
    /// Asynchronously filters the elements of an <see cref="IAsyncEnumerable{T}"/> based on a predicate.
    /// </summary>
    /// <typeparam name="T">The type of the elements in the enumerable.</typeparam>
    /// <param name="source">The source <see cref="IAsyncEnumerable{T}"/>.</param>
    /// <param name="predicate">A function to test each element for a condition.</param>
    /// <returns>An <see cref="IAsyncEnumerable{T}"/> that contains elements from the input sequence that satisfy the condition specified by <paramref name="predicate"/>.</returns>
    public static async IAsyncEnumerable<T> WhereAsync<T>(this IAsyncEnumerable<T> source, Func<T, bool> predicate)
    {
        await foreach (var item in source)
        {
            if (predicate(item))
            {
                yield return item;
            }
        }
    }

    /// <summary>
    /// Asynchronously filters the elements of an <see cref="IAsyncEnumerable{T}"/> based on a predicate.
    /// </summary>
    /// <typeparam name="T">The type of the elements in the enumerable.</typeparam>
    /// <param name="source">The source <see cref="IAsyncEnumerable{T}"/>.</param>
    /// <param name="predicate">A function to test each element for a condition.</param>
    /// <returns>An <see cref="IAsyncEnumerable{T}"/> that contains elements from the input sequence that satisfy the condition specified by <paramref name="predicate"/>.</returns>
    public static async IAsyncEnumerable<T> WhereAsync<T>(this IAsyncEnumerable<T> source, Func<T, Task<bool>> predicate)
    {
        await foreach (var item in source)
        {
            if (await predicate(item))
            {
                yield return item;
            }
        }
    }

    /// <summary>
    /// Asynchronously projects each element of an <see cref="IAsyncEnumerable{TSource}"/> into a new form.
    /// </summary>
    /// <typeparam name="TSource">The type of the elements in the source enumerable.</typeparam>
    /// <typeparam name="TResult">The type of the value returned by <paramref name="selector"/>.</typeparam>
    /// <param name="source">The source <see cref="IAsyncEnumerable{TSource}"/>.</param>
    /// <param name="selector">A transform function to apply to each element.</param>
    /// <returns>An <see cref="IAsyncEnumerable{TResult}"/> whose elements are the result of invoking the transform function on each element of <paramref name="source"/>.</returns>
    public static async IAsyncEnumerable<TResult> SelectAsync<TSource, TResult>(this IAsyncEnumerable<TSource> source, Func<TSource, TResult> selector)
    {
        await foreach (var item in source)
        {
            yield return selector(item);
        }
    }

    /// <summary>
    /// Asynchronously projects each element of an <see cref="IAsyncEnumerable{TSource}"/> into a new form.
    /// </summary>
    /// <typeparam name="TSource">The type of the elements in the source enumerable.</typeparam>
    /// <typeparam name="TResult">The type of the value returned by <paramref name="selector"/>.</typeparam>
    /// <param name="source">The source <see cref="IAsyncEnumerable{TSource}"/>.</param>
    /// <param name="selector">A transform function to apply to each element.</param>
    /// <returns>An <see cref="IAsyncEnumerable{TResult}"/> whose elements are the result of invoking the transform function on each element of <paramref name="source"/>.</returns>
    public static async IAsyncEnumerable<TResult> SelectAsync<TSource, TResult>(this IAsyncEnumerable<TSource> source, Func<TSource, Task<TResult>> selector)
    {
        await foreach (var item in source)
        {
            yield return await selector(item);
        }
    }

    /// <summary>
    /// Asynchronously projects each element of an <see cref="IAsyncEnumerable{T}"/> into a new form, allowing multiple parallel tasks.
    /// </summary>
    /// <typeparam name="T">The type of the elements in the source enumerable.</typeparam>
    /// <typeparam name="TResult">The type of the value returned by <paramref name="function"/>.</typeparam>
    /// <param name="items">The source <see cref="IAsyncEnumerable{T}"/>.</param>
    /// <param name="function">A function to apply to each element.</param>
    /// <param name="maxParallelTasks">The maximum number of parallel tasks to use. If null, the number of tasks will be equal to the number of processors on the machine.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken"/> that can be used to cancel the operation.</param>
    /// <returns>An <see cref="IAsyncEnumerable{TResult}"/> whose elements are the result of invoking the transform function on each element of <paramref name="items"/>.</returns>
    public static async IAsyncEnumerable<TResult> ParallelSelectAsync<T, TResult>(this IAsyncEnumerable<T> items, Func<T, Task<TResult>> function,
        int? maxParallelTasks = null, [EnumeratorCancellation] CancellationToken cancellationToken = default)
    {
        ArgumentNullException.ThrowIfNull(items);
        ArgumentNullException.ThrowIfNull(function);
        maxParallelTasks = Math.Max(maxParallelTasks ?? Environment.ProcessorCount, 0);
        Queue<Task<TResult>> taskQueue = [];
        await foreach (var item in items)
        {
            cancellationToken.ThrowIfCancellationRequested();
            var currentTasks = taskQueue.Where(x => !x.IsCompleted).ToList();
            if (currentTasks.Count > maxParallelTasks)
            {
                await Task.WhenAny(currentTasks).ConfigureAwait(false);
            }
            taskQueue.Enqueue(function(item));
            while (taskQueue.TryPeek(out var task) && task.IsCompleted)
            {
                yield return await taskQueue.Dequeue().ConfigureAwait(false);
            }
        }
        while (taskQueue.Count > 0)
        {
            yield return await taskQueue.Dequeue().ConfigureAwait(false);
        }
    }

}
#endif
