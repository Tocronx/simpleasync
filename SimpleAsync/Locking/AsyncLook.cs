﻿using System.Threading;
using System.Threading.Tasks;

namespace Tocronx.SimpleAsync;

/// <summary>
/// Simple lock implementation for use with async/await.
/// </summary>
public class AsyncLock
{
    private readonly SemaphoreSlim semaphoreSlim = new(1, 1);

    /// <summary>
    /// Get the lock async.
    /// </summary>
    /// <returns>The object representing the lock.</returns>
    public async Task<ILockObject> GetLookAsync()
    {
        await semaphoreSlim.WaitAsync().ConfigureAwait(false);
        return new LockObject(() => semaphoreSlim.Release());
    }

    /// <summary>
    /// Get the lock synchronously.
    /// </summary>
    /// <returns>The object representing the lock.</returns>
    public ILockObject GetLook()
    {
        semaphoreSlim.Wait();
        return new LockObject(() => semaphoreSlim.Release());
    }

}
