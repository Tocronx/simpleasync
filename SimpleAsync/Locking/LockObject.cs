﻿using System;

namespace Tocronx.SimpleAsync;

/// <summary>
/// Interface for the IDisposable implementing object holding the lock.
/// </summary>
public interface ILockObject : IDisposable { }

internal class LockObject : ILockObject
{
    private Action? disposeAction;

    public LockObject(Action disposeAction)
    {
        this.disposeAction = disposeAction;
    }

    public void Dispose()
    {
        disposeAction?.Invoke();
        disposeAction = null;
    }

}
