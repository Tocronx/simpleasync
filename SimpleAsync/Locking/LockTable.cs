﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Tocronx.SimpleAsync;

/// <summary>
/// LockTable classes to easily lock different values/objects of type T.
/// </summary>
/// <typeparam name="T">Type of the entries to lock.</typeparam>
public class LockTable<T> where T : notnull
{
    private class LockEntry : IDisposable
    {
        private readonly SemaphoreSlim semaphoreSlim = new(1, 1);
        public int Count { get; set; }
        public bool TryLock() => semaphoreSlim.Wait(0);
        public void Lock() => semaphoreSlim.Wait();
        public Task LockAsync() => semaphoreSlim.WaitAsync();
        public void Unlock() => semaphoreSlim.Release();
        public void Dispose() => semaphoreSlim.Dispose();
    }

    private readonly object lockObject = new();
    private readonly Dictionary<T, LockEntry> dictionary;


    /// <summary>
    /// Default constructor
    /// </summary>
    public LockTable() : this(null) { }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="comparer">A IEqualityComparer instance.</param>
    public LockTable(IEqualityComparer<T>? comparer)
    {
        dictionary = (comparer != null) ? new Dictionary<T, LockEntry>() : new Dictionary<T, LockEntry>(comparer);
    }

    /// <summary>
    /// Lock an item synchronously.
    /// </summary>
    /// <param name="item">Item to lock.</param>
    public void Lock(T item) => GetLockEntry(item).Lock();

    /// <summary>
    /// Lock an item asynchronously.
    /// </summary>
    /// <param name="item">Item to lock.</param>
    /// <returns>Task object to wait for obtaining the lock.</returns>
    public Task LockAsync(T item) => GetLockEntry(item).LockAsync();

    /// <summary>
    /// Try to lock an item synchronously.
    /// </summary>
    /// <param name="item">Item to lock.</param>
    /// <returns>Bool that indicates if the object was locked.</returns>
    public bool TryLock(T item)
    {
        var lockItem = GetLockEntry(item);
        if (lockItem.TryLock()) { return true; }
        CleanupLockEntry(item, false);
        return false;
    }

    /// <summary>
    /// Unlock an item synchronously.
    /// </summary>
    /// <param name="item">Item to unlock.</param>
    public void Unlock(T item) => CleanupLockEntry(item, true);

    /// <summary>
    /// Lock an item asynchronously.
    /// </summary>
    /// <param name="item">Item to lock.</param>
    /// <returns>Disposable ILockObject representing the lock of the item.</returns>
    public ILockObject GetLock(T item)
    {
        Lock(item);
        return new LockObject(() => Unlock(item));
    }

    /// <summary>
    /// Lock an item asynchronously.
    /// </summary>
    /// <param name="item">Item to lock.</param>
    /// <returns>Task to await the disposable ILockObject representing the lock of the item.</returns>
    public async Task<ILockObject> GetLockAsync(T item)
    {
        await LockAsync(item);
        return new LockObject(() => Unlock(item));
    }

    private LockEntry GetLockEntry(T item)
    {
        lock (lockObject)
        {
            if (!dictionary.TryGetValue(item, out var lockEntry))
            {
                lockEntry = new LockEntry();
                dictionary.Add(item, lockEntry);
            }
            lockEntry.Count++;
            return lockEntry;
        }
    }

    private void CleanupLockEntry(T item, bool unlock)
    {
        lock (lockObject)
        {
            var lockEntry = dictionary[item];
            lockEntry.Count--;
            if (unlock) { lockEntry.Unlock(); }
            if (lockEntry.Count == 0)
            {
                dictionary.Remove(item);
                lockEntry.Dispose();
            }
        }
    }

}