﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Tocronx.SimpleAsync;

/// <summary>
/// Utility class for executing asynchronous operations and automatically cancelling the previous operation.
/// </summary>
public class CancelableTaskRunner
{
    private readonly object lockObject = new();
    private readonly bool asCompletedOnCancelation;
    private CancellationTokenSource? currentCancellationTokenSource = null;

    /// <summary>
    /// Default constructor
    /// </summary>
    public CancelableTaskRunner()
    {
        asCompletedOnCancelation = true;
    }

    /// <summary>
    /// Constructor with parameter for the result state (throw cancelation exception or return as completed).
    /// </summary>
    /// <param name="asCompletedOnCancelation">Boolean value indicating if in the case of cancelation the result state should be completed or canceled.</param>
    public CancelableTaskRunner(bool asCompletedOnCancelation)
    {
        this.asCompletedOnCancelation = asCompletedOnCancelation;
    }

    /// <summary>
    /// Starts a new asynchronous function and cancels the previous one if still running.
    /// </summary>
    /// <param name="asyncFunction">Async function to execute.</param>
    /// <returns>Task object representing the asynchronous function.</returns>
    public Task RunAndCancelPrevious(Func<Task> asyncFunction)
    {
        return RunAndCancelPrevious(token => asyncFunction(), out var cancellationToken);
    }

    /// <summary>
    /// Starts a new asynchronous function and cancels the previous one if still running.
    /// </summary>
    /// <param name="asyncFunction">Async function to execute.</param>
    /// <param name="cancellationToken">CancellationToken object indicating if a cancelation was requested.</param>
    /// <returns>Task object representing the asynchronous function.</returns>
    public Task RunAndCancelPrevious(Func<Task> asyncFunction, out CancellationToken cancellationToken)
    {
        return RunAndCancelPrevious(token => asyncFunction(), out cancellationToken);
    }

    /// <summary>
    /// Starts a new asynchronous function and cancels the previous one if still running.
    /// </summary>
    /// <param name="asyncFunction">Async function to execute.</param>
    /// <returns>Task object representing the asynchronous function.</returns>
    public Task RunAndCancelPrevious(Func<CancellationToken, Task> asyncFunction)
    {
        return RunAndCancelPrevious(asyncFunction, out _);
    }

    /// <summary>
    /// Starts a new asynchronous function and cancels the previous one if still running.
    /// </summary>
    /// <param name="asyncFunction">Async function to execute.</param>
    /// <param name="cancellationToken">CancellationToken object indicating if a cancelation was requested.</param>
    /// <returns>Task object representing the asynchronous function.</returns>
    public Task RunAndCancelPrevious(Func<CancellationToken, Task> asyncFunction, out CancellationToken cancellationToken)
    {
        currentCancellationTokenSource?.Cancel();
        var cancellationTokenSource = new CancellationTokenSource();
        lock (lockObject)
        {
            currentCancellationTokenSource = cancellationTokenSource;
        }
        var token = cancellationTokenSource.Token;
        cancellationToken = token;
        return asyncFunction(token).ContinueWith((task) =>
        {
            lock (lockObject)
            {
                if (cancellationTokenSource == currentCancellationTokenSource)
                {
                    currentCancellationTokenSource = null;
                }
            }
            cancellationTokenSource.Dispose();
            return (!asCompletedOnCancelation && token.IsCancellationRequested) ? Task.FromCanceled(token) : Task.CompletedTask;
        }).Unwrap();
    }

    /// <summary>
    /// Starts a new asynchronous function with a result value and cancels the previous one if still running.
    /// </summary>
    /// <param name="asyncFunction">Async function to execute.</param>
    /// <returns>Task object for the result of asynchronous function.</returns>
    public Task<T?> RunAndCancelPrevious<T>(Func<Task<T?>> asyncFunction)
    {
        return RunAndCancelPrevious(token => asyncFunction(), out var cancellationToken);
    }

    /// <summary>
    /// Starts a new asynchronous function with a result value and cancels the previous one if still running.
    /// </summary>
    /// <param name="asyncFunction">Async function to execute.</param>
    /// <param name="cancellationToken">CancellationToken object indicating if a cancelation was requested.</param>
    /// <returns>Task object for the result of asynchronous function.</returns>
    public Task<T?> RunAndCancelPrevious<T>(Func<Task<T?>> asyncFunction, out CancellationToken cancellationToken)
    {
        return RunAndCancelPrevious(token => asyncFunction(), out cancellationToken);
    }

    /// <summary>
    /// Starts a new asynchronous function with a result value and cancels the previous one if still running.
    /// </summary>
    /// <param name="asyncFunction">Async function to execute.</param>
    /// <returns>Task object for the result of asynchronous function.</returns>
    public Task<T?> RunAndCancelPrevious<T>(Func<CancellationToken, Task<T?>> asyncFunction)
    {
        return RunAndCancelPrevious<T>(asyncFunction, out _);
    }

    /// <summary>
    /// Starts a new asynchronous function with a result value and cancels the previous one if still running.
    /// </summary>
    /// <param name="asyncFunction">Async function to execute.</param>
    /// <param name="cancellationToken">CancellationToken object indicating if a cancelation was requested.</param>
    /// <returns>Task object for the result of asynchronous function.</returns>
    public Task<T?> RunAndCancelPrevious<T>(Func<CancellationToken, Task<T?>> asyncFunction, out CancellationToken cancellationToken)
    {
        currentCancellationTokenSource?.Cancel();
        var cancellationTokenSource = new CancellationTokenSource();
        lock (lockObject)
        {
            currentCancellationTokenSource = cancellationTokenSource;
        }
        var token = cancellationTokenSource.Token;
        cancellationToken = token;
        return asyncFunction(token).ContinueWith((task) =>
        {
            lock (lockObject)
            {
                if (cancellationTokenSource == currentCancellationTokenSource)
                {
                    currentCancellationTokenSource = null;
                }
            }
            cancellationTokenSource.Dispose();
            if (token.IsCancellationRequested)
            {
                return asCompletedOnCancelation ? Task.FromResult<T?>(default) : Task.FromCanceled<T?>(token);
            }
            return task;
        }).Unwrap();
    }

}
