﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Tocronx.SimpleAsync;

/// <summary>
/// Utility class to execute asynchronous operations sequentially.
/// </summary>
public class SequentialTaskRunner
{
    private readonly object lockObject = new();
    private volatile int count = 0;

    /// <summary>
    /// Property for the awaitable Task instance representing the execution of the entire current sequence.
    /// </summary>
    public Task ExecutionTask { get; private set; } = Task.CompletedTask;

    /// <summary>
    /// Indicates if the task runner is idle. True if no uncompleted tasks exists and false if there are entries waiting or being processed.
    /// </summary>
    public bool IsIdle => ExecutionTask.IsCompleted || ExecutionTask.IsCanceled || ExecutionTask.IsFaulted;

    /// <summary>
    /// Indicates if the queue is empty. True if no tasks waiting to be processed otherwise false.
    /// </summary>
    public bool IsEmpty => count == 0;

    /// <summary>
    /// The count of tasks in the queue waiting for processing.
    /// </summary>
    public int Count => count;

    /// <summary>
    /// Enqueue a async function for sequential execution.
    /// </summary>
    /// <param name="function">Async function to execute.</param>
    /// <returns>Task allowing to await the execution.</returns>
    public Task Enqueue(Func<Task> function)
    {
        if (function is null) throw new ArgumentNullException(nameof(function));
        lock (lockObject)
        {
            Interlocked.Increment(ref count);
            var tempTask = ExecutionTask.ContinueWith(x =>
            {
                Interlocked.Decrement(ref count);
                return function();
            }, TaskContinuationOptions.ExecuteSynchronously).Unwrap();
            ExecutionTask = tempTask;
            return tempTask;
        }
    }

    /// <summary>
    /// Enqueue a async function with a result value for sequential execution.
    /// </summary>
    /// <param name="function">Async function to execute.</param>
    /// <returns>Task allowing to await the result.</returns>
    public Task<T> Enqueue<T>(Func<Task<T>> function)
    {
        if (function is null) throw new ArgumentNullException(nameof(function));
        lock (lockObject)
        {
            Interlocked.Increment(ref count);
            var tempTask = ExecutionTask.ContinueWith(x =>
            {
                Interlocked.Decrement(ref count);
                return function();
            }, TaskContinuationOptions.ExecuteSynchronously).Unwrap();
            ExecutionTask = tempTask;
            return tempTask;
        }
    }

    /// <summary>
    /// Enqueue a async function for sequential execution if the queue is empty.
    /// </summary>
    /// <param name="function">Async function to execute.</param>
    /// <returns>Task allowing to await the execution. Returns immediately if the queue is not empty.</returns>
    public Task EnqueueIfEmpty(Func<Task> function)
    {
        if (function is null) throw new ArgumentNullException(nameof(function));
        lock (lockObject)
        {
            if (IsEmpty)
            {
                Interlocked.Increment(ref count);
                var tempTask = ExecutionTask.ContinueWith(x =>
                {
                    Interlocked.Decrement(ref count);
                    return function();
                }, TaskContinuationOptions.ExecuteSynchronously).Unwrap();
                ExecutionTask = tempTask;
                return tempTask;
            }
            return Task.CompletedTask;
        }
    }

}
