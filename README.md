# SimpleAsync

Simple library with helpful classes/functions that make it easier to work with tasks and async/await. (SequentialTaskRunner, CancelableTaskRunner, AsyncLock, ...)